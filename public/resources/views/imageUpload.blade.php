



<!DOCTYPE html>
<html>
<style>
* {
  box-sizing: border-box;
}

body {
  margin: 0;
  font-family: Arial;
}

.header {
  text-align: center;
  padding: 32px;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 50%;
  padding: 10px;
}

.column img {
  margin-top: 12px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
<body>

<!-- Header -->
<div class="header">
  <h1>Image Grid</h1>
</div>

<!-- Photo Grid -->
<div class="row"> 
  <div class="column">
  <img src="/w3images/wedding.jpg" style="width:100%">
  <img src="/w3images/rocks.jpg" style="width:100%">
  <img src="/w3images/falls2.jpg" style="width:100%">
 
  </div>
  <div class="column">
  <img src="images/1_xj28wQAoHyc-WSzDImPMNQ.jpg" style="width:100%">
  <img src="/w3images/ocean.jpg" style="width:100%">
  <img src="/w3images/wedding.jpg" style="width:100%">

  </div>  
  <div class="column">
  <img src="/w3images/wedding.jpg" style="width:100%">
  <img src="/w3images/rocks.jpg" style="width:100%">
  <img src="/w3images/falls2.jpg" style="width:100%">
  <img src="/w3images/paris.jpg" style="width:100%">
    </div>
  <div class="column">
  <img src="/w3images/underwater.jpg" style="width:100%">
  <img src="/w3images/ocean.jpg" style="width:100%">
  <img src="/w3images/wedding.jpg" style="width:100%">
  </div>
</div>

</body>
</html>























<!DOCTYPE html>
<html>
<head>
   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
    
<body>
<div class="container">
      <div class="panel-body">
     
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        <img src="images/{{ Session::get('image') }}">
        @endif
    
       
    
        <form action="{{ route('image.upload.post') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
    
                <div class="col-md-6">
                    <input type="file" name="image" class="form-control">
                </div>
     
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
     
            </div>
        </form>
    
      </div>
    </div>
</div>
</body>
  
</html>