<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
  
class ImageUploadController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageUpload()
    {
        return view('imageUpload');
    }



    public function imageUpload1()
    {
        return view('imageview');
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageUploadPost(Request $request)
    {

        // dd("imageName");
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        $imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('images'), $imageName);
  
        /* Store $imageName name in DATABASE from HERE */
        // dd($imageName);
    
        return back()
            ->with('success','You have successfully upload image.')
            ->with('image',$imageName); 
        
    }



    public function imageUploadPost2(Request $request)
    {

    //   dd( $request->keyname);  
      $tag=$request->keyname;
      $url= 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=4780adc7dcb1e9a3852832777c9dfe48&tags='.$tag.'&gallery_id=&format=json&nojsoncallback=1';
      //dd($url);
    $data = json_decode(file_get_contents($url));
    $photos = $data->photos->photo; 
    $cart = array();
  
    foreach($photos as $photo) {
    $url = 'http://farm'.$photo->farm.'.staticflickr.com/'.$photo->server.'/'.$photo->id.'_'.$photo->secret.'.jpg';
    array_push($cart,$url);
    // echo'<img src="'.$url.'">';
    




    }
    return view('imageshow')
      ->with('cart',$cart); 
      
        
    }




    public function imageUploadPost1(Request $request)
    {
        $tag='cat';
        $url= 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=4780adc7dcb1e9a3852832777c9dfe48&tags='.$tag.'&gallery_id=&format=json&nojsoncallback=1';
        
      $data = json_decode(file_get_contents($url));
      $photos = $data->photos->photo; 
      $i=0;
      $cart = array();
foreach($photos as $photo) {
//    dd($i);
   if($i<30){
$url = 'http://farm'.$photo->farm.'.staticflickr.com/'.$photo->server.'/'.$photo->id.'_'.$photo->secret.'.jpg';

array_push($cart,$url);
    // dd($cart);

//  echo'<img src="'.$url.'">';
$i++;
   }
  

      } 
      return view('imageshow')
      ->with('cart',$cart);
      
        
    }

}