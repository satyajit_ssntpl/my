<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ClaintController;
use App\Http\Controllers\ImageUploadController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Route::post('/list',[HomeController::class,"list1"]);


Route::get('/save',[ClaintController::class,"create"]);
Route::get('/edit/{id}', [ClaintController::class,"edit"])->name('edit1');
Route::post('/save/data',[ClaintController::class,"add"])->name('add');

Route::get('image-upload2', [ ImageUploadController::class, 'imageUpload' ])->name('imageUpload');
Route::get('image-upload4', [ ImageUploadController::class, 'imageUpload1' ])->name('imagechose');

Route::get('image-upload1', [ ImageUploadController::class, 'imageUploadPost1' ]);
Route::post('image-upload3', [ ImageUploadController::class, 'imageUploadPost' ])->name('upload.post.image1');
Route::post('image-upload5', [ ImageUploadController::class, 'imageUploadPost2' ])->name('upload.post.image');
